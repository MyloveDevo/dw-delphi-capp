unit udllfuns_dbgridSelfDraw;

interface

uses
  SysUtils, Classes, Graphics, Forms, Dialogs, DB, ADODB, Grids,UpWWDbGrid;


{ 自定义画dbgrid.cell pdbgridSelfDraw.dll接口函数
}
//入口函数
procedure CalcCellColors_main(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); stdcall external '.\pdbgridSelfDraw.dll';

implementation

end.
