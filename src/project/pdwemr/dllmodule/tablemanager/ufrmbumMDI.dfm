inherited frmbumMDI: TfrmbumMDI
  Left = 71
  Top = 132
  Caption = #37096#38376#31649#29702
  ClientHeight = 667
  ClientWidth = 1234
  PixelsPerInch = 96
  TextHeight = 13
  inherited rztlbr2: TRzToolbar
    Width = 1234
    RowHeight = 29
    ToolbarControls = (
      btnprint
      btnquit)
    inherited btnprint: TRzToolButton
      Left = 4
      Top = 3
    end
    inherited btnquit: TRzToolButton
      Left = 84
      Top = 3
    end
  end
  inherited pmain: TUPanel
    Width = 1234
    Height = 631
    inherited tvtree: TTreeView
      Width = 1005
      Height = 629
    end
    inherited p2: TPanel
      Height = 629
      inherited p1: TUpAdvToolPanel
        Height = 259
        inherited btn2: TUpSpeedButton
          Left = 9
          Top = 146
        end
        inherited btn3: TUpSpeedButton
          Left = 114
          Top = 146
        end
        inherited btn4: TUpSpeedButton
          Top = 181
        end
        inherited btn5: TUpSpeedButton
          Left = 114
          Top = 181
          Width = 99
        end
        inherited lbl4: TLabel
          Caption = #37096#38376#20195#21495#65306
        end
        inherited lbl3: TLabel
          Left = 8
          Width = 65
          Caption = #37096#38376#21517#31216#65306
        end
        inherited edtpaixh: tuprzspinedit
          Width = 143
        end
        object chkcanjps: TUpCheckBox
          Left = 73
          Top = 108
          Width = 90
          Height = 16
          Caption = #21442#21152#35780#23457
          Checked = True
          State = cbChecked
          TabOrder = 3
          AutoInit = False
          HistoryValue = False
          HistoryValueOnOff = True
        end
      end
      inherited p3: TUpAdvToolPanel
        Top = 260
        Height = 0
      end
      object updvtlpnl1: TUpAdvToolPanel
        Left = 1
        Top = 260
        Width = 225
        Height = 368
        Align = alClient
        BackgroundTransparent = False
        BackGroundPosition = bpTopLeft
        BorderWidth = 1
        Button3D = False
        HoverButtonColor = 16571329
        HoverButtonColorTo = 16565398
        DownButtonColor = 16571329
        DownButtonColorTo = 16565398
        CaptionButton = False
        Color = 16571329
        ColorTo = 16571329
        GradientDirection = gdVertical
        DockDots = True
        CanSize = False
        Caption = #37096#38376#39046#23548#35774#32622
        CaptionGradientDirection = gdVertical
        FocusCaptionFontColor = clBlack
        FocusCaptionColor = 16571329
        FocusCaptionColorTo = 16565398
        NoFocusCaptionFontColor = clBlack
        NoFocusCaptionColor = 16571329
        NoFocusCaptionColorTo = 16565398
        CloseHint = 'Close panel'
        LockHint = 'Lock panel'
        UnlockHint = 'Unlock panel'
        Sections = <>
        SectionLayout.CaptionColor = 16244422
        SectionLayout.CaptionColorTo = 14060643
        SectionLayout.CaptionFontColor = 8661248
        SectionLayout.CaptionRounded = False
        SectionLayout.Corners = scRectangle
        SectionLayout.BackGroundColor = 16248798
        SectionLayout.BackGroundColorTo = 16242365
        SectionLayout.BorderColor = clWhite
        SectionLayout.BorderWidth = 1
        SectionLayout.BackGroundGradientDir = gdVertical
        SectionLayout.Indent = 4
        SectionLayout.Spacing = 4
        SectionLayout.ItemFontColor = 11876608
        SectionLayout.ItemHoverTextColor = 11876608
        SectionLayout.ItemHoverUnderline = True
        SectionLayout.UnderLineCaption = False
        ShowCaptionBorder = False
        ShowClose = False
        ShowLock = False
        Style = esCustom
        Version = '1.5.0.1'
        OrgHeight = 0
        object upnl1: TUPanel
          Left = 2
          Top = 25
          Width = 221
          Height = 35
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object btnaddzhuanj: TUpSpeedButton
            Left = 7
            Top = 4
            Width = 27
            Height = 26
            Flat = True
            Glyph.Data = {
              7A030000424D7A030000000000007A0200002800000010000000100000000100
              08000000000000010000120B0000120B0000910000009100000000000000FFFF
              FF00FF00FF00FAF7F200DDDDD400EEEEE700F7F7F400F8FAF700F8FAFF005E72
              AB00828FB200F4F7FF0041579D004C66B7004B64B5007B93DE00596A9F006779
              B2006375AB00E5E9F600FBFCFF00FAFBFE005875DA007D97F7007790EB007B93
              E500768BD1009FB2F600A0B2F200C7D3FF00B6C1E700CCD7FF004164FB004164
              FA004769FA003C59D400486AF8005475FF005978FE005B7BFF005D78EA006983
              EF00738EFF00647BDD00738EF8006B82E0007B95FC00819AFF00849DFF00839C
              FA0095AAFC009FB2FB00A9BAFC00B2C1FC00BAC4EB00CBD5FF00CAD4FE00C5CF
              F700C5CEF400D1DAFF00E6EBFF00D1D5E500DCE0F000E9EDFC001133DD000F2D
              C2000A1F83000C2391001332CB001C3FE3001D3EDE002449FB002647E9002B4C
              EB003256FC003255FB00304FE200365AFA00304FDE003658F2003C5EFF003E5F
              FA003E60FA004060FC004062FC004264FC004668FE004B6BFC004F6FFE005070
              FB005E7BFE005C79F7007992FF008FA4FF00EDF0FF00021FCE00041FC2000315
              81000E30EF000F32FE001135FF001135FC000F2DDE000F30DE001233E6001130
              D8000E28AD00163BFC00173BFF001331CB002447FB002548FE00001CD1000019
              CE00001AC700011FDE00011CCC00021DE300021CCF00041FD5000628FF000628
              FE00051FCC00051FCB000D2DF2000E2EF2001132FE00001DFE000015CE000013
              C0000421FE000013FA000016F7000013F0000016EF000013E7000013DE00000F
              C9000012C000000FDE00000CAB000000810000007D00FBFBFF00FEFEFF000202
              0202028D8D8D8D8D8D02020202020202028C8C6A0E11120C428E8E0202020202
              8C6019063E361E3D0410618E0202028C7A1C01286971726D2B030A618E02028C
              17904988734C4E5F8A2307098E027F68085B858662010B6780812D05438E7F27
              3C63827E6E010148667644130D8E7F5C1D795001010101010146743A1A8E7F2F
              1F6B580101010101014570390F8E7F2A5E544B535701016F7D75403F168E7F4A
              8F32514D5501016484872915418E027F3401312452265678834F14188C02027F
              213801335947656C5A011B7B8C0202027F2235015E3B373C012C778C02020202
              027F7F202E5D30257C8C8C02020202020202027F7F7F7F8B890202020202}
            alignleftorder = 0
            alignrightorder = 0
          end
          object btndelzhuanj: TUpSpeedButton
            Left = 39
            Top = 4
            Width = 27
            Height = 26
            Flat = True
            Glyph.Data = {
              96030000424D9603000000000000960200002800000010000000100000000100
              08000000000000010000120B0000120B0000980000009800000000000000FFFF
              FF00FF00FF00FBF9F500E5E5DE00F2F2ED00F9F9F700FAFBF900FCFDFF00FBFC
              FE005B72B1006780C700798BBD007D8EBD009AA5C300738EE300667EC50094A9
              EB0094A9E6007484B3008FA2DC008192C3000030E200002ED4000130DE000130
              DA000231DC000331D8000838EB000A38E3001E45D00017349B00224CE5001D40
              BF001939A700244AD700244BD7003B64FC004B71FD005175FB005073F4005377
              FB005478FB005B7EFB005C7EFC006283FB005674DE006585FA006B8AFC006F8E
              FF007694FF0089A2FF00839BF3008199EE007E94E5008CA4FA00859AE70099AF
              FF0095ACF90090A6F0009CB1FF009BB0FB00AABCFD00A5B6F500B3C3FC00B7C6
              FE00B3C3F800B4C3F500BBC9FD00C6CFED00F1F4FF00EBEEF8000033FF000032
              FE000031F5000030F1000030EE000030E800002DE500002DD900002CD8000130
              E200032FD9000739FF000738FB000738F8000838F1000A3AFE000C34DF000C34
              D0000D34D8000D34D700092799001040FE001645FE001B46F5001E4BFE00224F
              FD002450FD002349EC00234CEC002249E1002955FD002A55FF002D58FD003A61
              FF003C64FD003B62F7004269FC004367F000496DFE00587AFE005A7CFD005F80
              FC006585FD00728EFD007994FE007792F9007891EF008CA4FF0092A8FF0094AA
              FD00A5B7FF00C3CFFD00D4DDFF00C9D1F000D7DFFF00D6DEFE00D2DAF900D8E0
              FF00D2D9F700DCE3FF00ECF0FF00DCDFEB00EEF1FD00FAFBFF00002AF9000027
              E6000025CF000023CE000533FE00E4E7F4000025FB000025F400001ED5000021
              EB00001EE6000019BD000000990000009500FCFCFF00FEFEFF00020202020294
              9494949494020202020202020293932110150D0A1F9595020202020293591106
              8D7D458504135C95020202935A430176654F172336030E5C950202933A976D89
              4E5118508A2E070C9502496487758F4A4B4C4D16198B38052295493284608C57
              5455561C1D5224470B9549787C5D33010101010101351B821495493981664101
              01010101013F1A801295497746707173726F6E696B6320860F954926963E2A29
              6C685E48889134091E95024944013D2F276A62538E28083B930202492B7F0140
              302561677401425B93020202492D7B0146837E8401375893020202020249492C
              797A3C315F939302020202020202024949494992900202020202}
            alignleftorder = 0
            alignrightorder = 0
          end
        end
        object dbgbumlingd: TUpWWDbGrid
          Left = 2
          Top = 60
          Width = 221
          Height = 306
          Selected.Strings = (
            'xingm'#9'16'#9#22995#21517#9#9
            'denglzh'#9'11'#9#30331#24405#36134#21495#9'F'#9)
          IniAttributes.Delimiter = ';;'
          TitleColor = 16571329
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = dsbumlingd
          ImeName = #20013#25991'('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = GB2312_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -13
          TitleFont.Name = #23435#20307
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = True
          UseTFields = False
        end
      end
    end
  end
  object dsbumlingd: TUpDataSource
    DataSet = qrybumlingd
    Left = 96
    Top = 480
  end
  object qrybumlingd: TUpAdoQuery
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=admin;Persist Security Info=True;Us' +
      'er ID=sa;Initial Catalog=pdfreview;Data Source=127.0.0.1,7788'
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from v_yuang')
    Left = 64
    Top = 480
  end
end
