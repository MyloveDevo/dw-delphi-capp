//*******************************************************************
//  pmyhisgroup  pdbgridSelfDraw   
//                                          
//  创建: changym by 2012-08-13 
//        changup@qq.com                    
//  功能说明:                                            
//      系统管理dbg自画函数
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udbgselfdraw_xitgl;

interface

uses
  SysUtils,
  DB,
  Dialogs,
  Graphics,
  ADODB,
  Grids,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid;

procedure dbgCalcCellColor_xitgl_daohlqxdbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

implementation

//系统管理; 导航栏权限分类dbg
procedure dbgCalcCellColor_xitgl_daohlqxdbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //是否是组记录
  if TUpWWDbGrid(Sender).DataSource.DataSet.FieldByName('zubs').AsInteger = 1 then
  begin
    ABrush.Color := clGradientInactiveCaption;
    AFont.Style := [fsBold];
    
    //当前条目高亮显示
    if Highlight then
    begin
      AFont.Color := clActiveCaption;
      AFont.Style := [fsBold];
    end;
  end;

end;

end.
