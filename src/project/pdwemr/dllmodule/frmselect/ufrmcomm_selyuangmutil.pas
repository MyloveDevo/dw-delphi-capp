unit ufrmcomm_selyuangmutil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbselectbase, frxExportXLS, frxClass, frxExportPDF, Menus,
  UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB, ADODB, UpAdoTable,
  UpAdoQuery, Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid, StdCtrls, UpEdit,
  UpLabel, ToolPanels, UpAdvToolPanel, Buttons, UpSpeedButton, ExtCtrls,
  UPanel, ComCtrls, UpTreeView, ImgList, udbtree;

type          
  Tfun_selRecord = procedure(bianm : integer) of object;
  
  Tfrmselyuangmutil = class(Tfrmdbselectbase)
    qryxiangmtree: TUpAdoQuery;
    imglstilimglisttree: TImageList;
    p1: TUpAdvToolPanel;
    tvxiangm: TUpTreeView;
    btn1: TUpSpeedButton;
    procedure tvxiangmGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure tvxiangmChange(Sender: TObject; Node: TTreeNode);
    procedure edtseljianpKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnselectClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure dbgDblClick(Sender: TObject);
    procedure edtseljianpKeyPress(Sender: TObject; var Key: Char);
  private
    dbtree : TDBTree;
    
    function getnodename_showtreenode() : string;
    //显示目录树获取节点显示名称回调函数

    procedure initDbgRecordUnCheckbox();
    //初始化Dbg.checkbox为未选择状态

    procedure procSelectedRecord();
    //处理选中的记录
  
  protected                                                    
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;

  public     
    FFun_SelRecord : Tfun_selRecord;
    //选择检查项后的处理
  end;

{
var
  frmselyuangmutil: Tfrmselyuangmutil;
}

implementation

{$R *.dfm}

{ Tfrmdbselectbase2 }

function Tfrmselyuangmutil.Execute_dll_showmodal_after: Boolean;
begin
  result := false;
  
  if qry.isEmpty then exit;

  if ModalResult=mrok then
  begin
    procSelectedRecord();
  
    Result := True;
  end;
end;

function Tfrmselyuangmutil.Execute_dll_showmodal_before: Boolean;
begin
  //不采用父类的处理方法
  //inherited;
                 
  //显示项目树
  if not Assigned(dbtree) then
  begin
    //创建树操作对象
    dbtree := TDBTree.Create(qryxiangmtree, 'jc_bum', tvxiangm, dllparams.padoconn);
    dbtree.fgetnodename_showtreenode := getnodename_showtreenode;
  end;

  dbtree.wherestr:= ' where zhuangt=1 ';
  //显示树
  dbtree.showtree;

  result := true;
end;

function Tfrmselyuangmutil.getnodename_showtreenode: string;
begin
  Result := qryxiangmtree.fieldbyname('mingc').AsString;
end;

procedure Tfrmselyuangmutil.tvxiangmGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  { 改变节点图标
  }
  if Node.Selected then
  begin
    Node.ImageIndex := 1;
  end
  else
  begin
    Node.ImageIndex := 2;
  end;
end;

procedure Tfrmselyuangmutil.tvxiangmChange(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  //树当前选中节点改变,通知qry改变当前记录
  dbtree.treechange(node);

  { 过滤项目
  }
  { 只在选中分类下的项目中过滤}
  if tvxiangm.Selected<>nil then
  begin
    qry.OpenSql(format('select * from v_yuang' +
      ' where bumbm=%d' +
      '   and zhuangt=1',
       [qryxiangmtree.getInteger('bianm')]));
  end
  else
  begin
    qry.OpenSql('select * from v_yuang where 1=2');
  end;
  
  { 初始化dbg.cehckbox选择状态为未选择状态
  }
  initDbgRecordUnCheckbox();
end;

procedure Tfrmselyuangmutil.edtseljianpKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //inherited;

  if key=13 then
  begin
    if trim(edtseljianp.Text)<>'' then
    begin
      qry.OpenSql(format('select * from v_yuang' +
        ' where denglzh like ''%s%s%s''' +
        '   and zhuangt=1',
        ['%', trim(edtseljianp.Text), '%']));
        
      { 初始化dbg.cehckbox选择状态为未选择状态
      }
      initDbgRecordUnCheckbox();
    end;
  end;
end;

procedure Tfrmselyuangmutil.btnselectClick(Sender: TObject);
var
  key: Word;
begin
  inherited;

  key:= 13;
  edtseljianpKeyUp(edtseljianp, key, []);
end;

procedure Tfrmselyuangmutil.procSelectedRecord;
begin
  qry.DisableControls;
  qry.First;
  while not qry.Eof do
  begin
    if qry.getInteger('zhuangt')=1 then
      if assigned(FFun_SelRecord) then
        FFun_SelRecord(qry.getInteger('bianm'));
      
    qry.Next;
  end;
  qry.First;
  qry.EnableControls;
end;

procedure Tfrmselyuangmutil.btn1Click(Sender: TObject);
begin
  inherited;

  if qry.isEmpty then exit;
  
  //确认并继续处理
  procSelectedRecord();
end;

procedure Tfrmselyuangmutil.dbgDblClick(Sender: TObject);
begin
  //inherited;

end;

procedure Tfrmselyuangmutil.initDbgRecordUnCheckbox;
begin       
  { 借用zhuangt字段做多选框，初始化为未选
  }
  qry.first;
  while not qry.Eof do
  begin
    qry.Edit;
    qry.FieldByName('zhuangt').AsInteger := 0;
    qry.Post;
    
    qry.Next;
  end;
  qry.first;
end;

procedure Tfrmselyuangmutil.edtseljianpKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if key='''' then
  begin
    key:= chr(0);
    exit;
  end;
end;

end.
